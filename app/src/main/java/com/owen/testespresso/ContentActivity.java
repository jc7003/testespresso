package com.owen.testespresso;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ContentActivity extends AppCompatActivity {

    private EditText mEdName;
    private EditText mEdMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        mEdName = (EditText) findViewById(R.id.ed_name);
        mEdMsg = (EditText) findViewById(R.id.ed_msg);
    }

    public void onClick(View view){
        String name = mEdName.getText().toString();
        String msg = mEdMsg.getText().toString();

        String toastMsg = "name:" + name + " , msg:" + msg;

        Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
    }
}
